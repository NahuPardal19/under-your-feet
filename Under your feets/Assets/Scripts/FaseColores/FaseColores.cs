using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FaseColores : MonoBehaviour
{
   
    public GameObject[] gameObjectsPurple;
    public GameObject[] gameObjectsYellow;
    public GameObject[] gameObjectsGreen;
    public GameObject[] gameObjectsBlue;
    public GameObject boton;
    private int cont;
    float tiempoRestante;
    public TMPro.TMP_Text textoTiempo;
    private bool FaseColorAct;
    public Light luzBoton;
    public Light luzBotonroja;
    public AudioListener audio;
    void Start()
    {
        cont = 0;
        FaseColorAct = false;
    }

 
    void Update()
    {
        gameObjectsPurple = GameObject.FindGameObjectsWithTag("Purple");
        gameObjectsYellow = GameObject.FindGameObjectsWithTag("Yellow");
        gameObjectsGreen = GameObject.FindGameObjectsWithTag("Green");
        gameObjectsBlue = GameObject.FindGameObjectsWithTag("Blue");

        if (gameObjectsPurple.Length == 10  && gameObjectsYellow.Length== 10 && gameObjectsGreen.Length == 12 && gameObjectsBlue.Length == 10 && cont == 0)
        {
            AudioListener.volume = 0;
            boton.tag = "boton";
            cont = 1;
            textoTiempo.text = "";
            luzBotonroja.enabled = false;
            luzBoton.enabled = true;
           FaseColorAct = false;
        }
        if (tiempoRestante == 1 && FaseColorAct)
        {
            AudioListener.volume = 0;
            SceneManager.LoadScene("EscenaMenu");
        }


    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 80)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            textoTiempo.text = "Tiempo: " + tiempoRestante.ToString();
            Debug.Log("Restan " + tiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("jugador") == true)
        {
            AudioListener.volume = 1;
            StartCoroutine(ComenzarCronometro(2000));
            GestorDeAudio.instancia.ReproducirSonido("Musica2");
            FaseColorAct = true;
        }
   
    }
}
