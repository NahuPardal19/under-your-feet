using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boton : MonoBehaviour
{
    public Transform tpInicio;
    public GameObject jugador;

    
    void Start()
    {
        AudioListener.volume = 0;
    }

    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.CompareTag("bala4") && this.gameObject.tag=="boton")
        {
            jugador.transform.position = tpInicio.transform.position;
            Destroy(collision.gameObject);
        }
    }
}
