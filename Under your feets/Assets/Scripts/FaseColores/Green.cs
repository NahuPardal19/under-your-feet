using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Green : MonoBehaviour
{
    public Renderer render;
    public Color color1;
    public Color color2;
    public Color color3;
    public Color colortp;
    public GameObject Habilidad;
    void Start()
    {

    }


    void Update()
    {

    }

    private void cambiamosColor1()
    {
        render = GetComponent<Renderer>();
        render.material.color = color1;
    }
    private void cambiamosColor2()
    {
        render = GetComponent<Renderer>();
        render.material.color = color2;
    }
    private void cambiamosColor3()
    {
        render = GetComponent<Renderer>();
        render.material.color = color3;
    }
    private void cambiamosColortp()
    {
        render = GetComponent<Renderer>();
        render.material.color = colortp;
    }




    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala1"))
        {
            GestorDeAudio.instancia.ReproducirSonido("Pintar");
            cambiamosColor1();
            this.gameObject.tag = "Default";
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("bala2"))
        {
            GestorDeAudio.instancia.ReproducirSonido("Pintar");
            cambiamosColor2();
            this.gameObject.tag = "Default";
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("bala3"))
        {
            GestorDeAudio.instancia.ReproducirSonido("Pintar");
            cambiamosColor3();
            this.gameObject.tag = "Green";
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("balatp"))
        {
            GestorDeAudio.instancia.ReproducirSonido("Pintar");
            cambiamosColortp();
            this.gameObject.tag = "Default";
            Destroy(collision.gameObject);
        }


    }
}
