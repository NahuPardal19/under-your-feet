using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PisoVerde : MonoBehaviour
{
    public Animator animator;
    public GameObject piso;
    public Material cambiocolor1;
    public Material cambiocolor2;
    float tiempoRestante;
    public GameObject jugador;
    public MeshRenderer pisovioleta;
    public MeshRenderer pisoamarillo;
    public MeshRenderer pisoverde;
    public MeshRenderer pisoazul;
    void Start()
    {
    }


    void Update()
    {
        if(tiempoRestante == 1)
        {
            jugador.tag = "jugador";
            pisoamarillo.enabled = true;
            pisovioleta.enabled = true;
            pisoverde.enabled = true;
            pisoazul.enabled = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Renderer render = piso.GetComponent<Renderer>();
        if (collision.gameObject.CompareTag("jugador") == true)
        {
            animator.Play("Verde");
            jugador.tag = "bot";
            render.material = cambiocolor2;
            pisoamarillo.enabled = false;
            pisovioleta.enabled = false;
            pisoverde.enabled = false;
            pisoazul.enabled = false;
            StartCoroutine(ComenzarCronometro(7));
        }
      
    }
    //private void OnCollisionExit(Collision collision)
    //{
    //    Renderer render = piso.GetComponent<Renderer>();
    //    if (collision.gameObject.CompareTag("jugador") == true)
    //    {
         
    //        render.material = cambiocolor1;
    //    }

    //}
    public IEnumerator ComenzarCronometro(float valorCronometro = 80)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }
}
