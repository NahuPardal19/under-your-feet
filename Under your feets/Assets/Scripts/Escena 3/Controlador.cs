using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controlador : MonoBehaviour
{
    public GameObject[] gameObjectsPurple;
    public GameObject[] gameObjectsYellow;
    public GameObject[] gameObjectsGreen;
    public GameObject[] gameObjectsBlue;
    private int cont;
    float tiempoRestante;
    public TMPro.TMP_Text textoTiempo;

    void Start()
    {
        cont = 0;
        StartCoroutine(ComenzarCronometro(1000));
    }

 
    void Update()
    {
        gameObjectsPurple = GameObject.FindGameObjectsWithTag("Purple");
        gameObjectsYellow = GameObject.FindGameObjectsWithTag("Yellow");
        gameObjectsGreen = GameObject.FindGameObjectsWithTag("Green");
        gameObjectsBlue = GameObject.FindGameObjectsWithTag("Blue");

        if (gameObjectsPurple.Length == 7 && gameObjectsYellow.Length == 7 && gameObjectsGreen.Length == 8 && gameObjectsBlue.Length == 8 && cont == 0)
        {
            cont = 1;
            textoTiempo.text = "";
            SceneManager.LoadScene("EscenaMenu");
        }
        if (tiempoRestante == 1)
        {
            SceneManager.LoadScene("EscenaMenu");
        }
    }
    public IEnumerator ComenzarCronometro(float valorCronometro = 80)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            textoTiempo.text = "Tiempo: " + tiempoRestante.ToString();
            Debug.Log("Restan " + tiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }
}
