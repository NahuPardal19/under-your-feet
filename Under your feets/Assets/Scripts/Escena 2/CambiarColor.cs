using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiarColor : MonoBehaviour
{
    public Renderer render;
    public Color color1;
    public Color color2;
    public Color color3;
    public Color colortp;
    public GameObject jugador;
    void Start()
    {
        
    }

    void Update()
    {
        if (jugador.gameObject.tag == "municionBlue")
        {
            render = GetComponent<Renderer>();     
            render.material.color = color1;
      
        }
        else if (jugador.gameObject.tag == "municionPurple")
        {
            render = GetComponent<Renderer>();
            render.material.color = color2;
        }
        else if (jugador.gameObject.tag == "municionYellow")
        {
            render = GetComponent<Renderer>();
            render.material.color = color3;
        }
        else if (jugador.gameObject.tag == "municionGreen")
        {
            render = GetComponent<Renderer>();
            render.material.color = colortp;
        }

    }
}
