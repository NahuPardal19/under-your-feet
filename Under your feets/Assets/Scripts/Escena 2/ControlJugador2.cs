using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador2 : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    private int cont;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    private Rigidbody rb;
    public Camera camaraPrimeraPersona;
    public int contColores;
    public GameObject jugador;
    public Transform tp;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        Cursor.lockState = CursorLockMode.Locked;
        contColores = 0;
    }


    void Update()
    {


        if (Input.GetKeyDown("escape"))
        {
            SceneManager.LoadScene("EscenaMenu");
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            Cursor.lockState = CursorLockMode.None;
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (EstaEnPiso())
            {
                rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            }

        }

    }
    private void FixedUpdate()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);


    }
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if ((collision.gameObject.CompareTag("Finish") || collision.gameObject.CompareTag("Purple") || collision.gameObject.CompareTag("Yellow") || collision.gameObject.CompareTag("Green") || collision.gameObject.CompareTag("Blue")) && (this.gameObject.tag == "municionBlue"))
        {
            jugador.transform.position = tp.transform.position;
            Debug.Log("Violet");
            this.gameObject.tag = "municionPurple";
        } 
        else if ((collision.gameObject.CompareTag("Finish") || collision.gameObject.CompareTag("Purple") || collision.gameObject.CompareTag("Yellow") || collision.gameObject.CompareTag("Green") || collision.gameObject.CompareTag("Blue")) && (this.gameObject.tag == "municionPurple"))
        {
            Debug.Log("Amarillo");
            jugador.transform.position = tp.transform.position;
            this.gameObject.tag = "municionYellow";
        }

        else if ((collision.gameObject.CompareTag("Finish") || collision.gameObject.CompareTag("Purple") || collision.gameObject.CompareTag("Yellow") || collision.gameObject.CompareTag("Green") || collision.gameObject.CompareTag("Blue")) && (this.gameObject.tag == "municionYellow"))
        {
            jugador.transform.position = tp.transform.position;
            Debug.Log("Verde");
            this.gameObject.tag = "municionGreen";
        }

        else if (((collision.gameObject.CompareTag("Finish")) || (collision.gameObject.CompareTag("Purple")) || (collision.gameObject.CompareTag("Yellow")) || (collision.gameObject.CompareTag("Green")) || (collision.gameObject.CompareTag("Blue"))) && (this.gameObject.tag == "municionGreen"))
        {
            jugador.transform.position = tp.transform.position;
            Debug.Log("Azul");
            this.gameObject.tag = "municionBlue";
        }


    }

}
