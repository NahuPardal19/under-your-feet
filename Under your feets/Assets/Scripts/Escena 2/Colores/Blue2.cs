using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blue2 : MonoBehaviour
{
    public Renderer render;
    public Color color1;
    public Color color2;
    public Color color3;
    public Color colortp;
    public GameObject Habilidad;
    void Start()
    {
        
    }

  
    void Update()
    {
        
    }
    private void cambiamosColor1()
    {
        render = GetComponent<Renderer>();
        render.material.color = color1;
    }
    private void cambiamosColor2()
    {
        render = GetComponent<Renderer>();
        render.material.color = color2;
    }
    private void cambiamosColor3()
    {
        render = GetComponent<Renderer>();
        render.material.color = color3;
    }
    private void cambiamosColortp()
    {
        render = GetComponent<Renderer>();
        render.material.color = colortp;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("municionPurple"))
        {
            cambiamosColor1();
            this.gameObject.tag = "Finish";
        }
        if (collision.gameObject.CompareTag("municionYellow"))
        {
            cambiamosColor2();
            this.gameObject.tag = "Finish";
        }
        if (collision.gameObject.CompareTag("municionGreen"))
        {
            cambiamosColor3();
            this.gameObject.tag = "Finish";
        }
        if (collision.gameObject.CompareTag("municionBlue"))
        {
            cambiamosColortp();
            this.gameObject.tag = "Blue";
        }


    }
}
