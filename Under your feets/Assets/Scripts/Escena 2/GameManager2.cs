using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager2 : MonoBehaviour
{
    public GameObject[] gameObjectsPurple;
    public GameObject[] gameObjectsYellow;
    public GameObject[] gameObjectsGreen;
    public GameObject[] gameObjectsBlue;
    private int cont;
    float tiempoRestante;
    public TMPro.TMP_Text textoTiempo;

    void Start()
    {
        StartCoroutine(ComenzarCronometro(1000));
    }

    void Update()
    {
        gameObjectsPurple = GameObject.FindGameObjectsWithTag("Purple");
        gameObjectsYellow = GameObject.FindGameObjectsWithTag("Yellow");
        gameObjectsGreen = GameObject.FindGameObjectsWithTag("Green");
        gameObjectsBlue = GameObject.FindGameObjectsWithTag("Blue");

        if (gameObjectsPurple.Length == 11 && gameObjectsYellow.Length == 11 && gameObjectsGreen.Length == 10 && gameObjectsBlue.Length == 10)
        {
            cont = 1;
            textoTiempo.text = "";
            SceneManager.LoadScene("EscenaMenu");
        }
        if (tiempoRestante == 1)
        {
            SceneManager.LoadScene("EscenaMenu");
        }
  
    }
    public IEnumerator ComenzarCronometro(float valorCronometro = 80)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            textoTiempo.text = "Tiempo: " + tiempoRestante.ToString();
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }
}
