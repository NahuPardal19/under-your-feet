using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBots : MonoBehaviour
{
    public GameObject[] gameObjectsPrueba;
    public GameObject[] bots;
    public GameObject[] municion;
    public SphereCollider ColliderInicio;
    float tiempoRestante;
    public TMPro.TMP_Text textoTiempo;
    private bool FaseBotsact;
    public Transform tpInicio;
    public GameObject jugador;
    public AudioListener audio;
    void Start()
    {
        FaseBotsact = false;
      AudioListener.volume=0;
}

    void Update()
    {
        gameObjectsPrueba = GameObject.FindGameObjectsWithTag("Prueba");
        if (gameObjectsPrueba.Length == 0)
        {
            ColliderInicio.enabled = true;
        }
        if (tiempoRestante == 1 && FaseBotsact)
        {
            AudioListener.volume = 0;
            jugador.transform.position = tpInicio.transform.position;
            textoTiempo.text = "";
           FaseBotsact = false;
        }
    }


    private void GenerarEnemigos()
    {
        float rndx = Random.Range(-35f, -130f);
        float rndz = Random.Range(-35f, 35f);
        int EnemigoElegido = Random.Range(0, bots.Length);
        Vector3 posicionRandom = new Vector3(rndx, 1.5f, rndz);

        Instantiate(bots[EnemigoElegido], posicionRandom, bots[EnemigoElegido].gameObject.transform.rotation);
    }
    private void GenerarMunicion()
    {
        float rndx = Random.Range(-35f, -130f);
        float rndz = Random.Range(-35f, 35f);
        int EnemigoElegido = Random.Range(0, municion.Length);
        Vector3 posicionRandom = new Vector3(rndx, 1.5f, rndz);

        Instantiate(municion[EnemigoElegido], posicionRandom, municion[EnemigoElegido].gameObject.transform.rotation);
    }
    public IEnumerator ComenzarCronometro(float valorCronometro = 80)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            textoTiempo.text = "Tiempo: " + tiempoRestante.ToString();
            Debug.Log("Restan " + tiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("jugador") == true)
        {
            AudioListener.volume = 1;
            InvokeRepeating("GenerarEnemigos", 3.0f, 1.5f);
            InvokeRepeating("GenerarMunicion", 3.0f, 10f);
            StartCoroutine(ComenzarCronometro(30));
            GestorDeAudio.instancia.ReproducirSonido("Musica");
            FaseBotsact = true; 

        }
    }

}
