using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotYellow : MonoBehaviour
{
    private GameObject jugador;
    public int rapidez;
    public GameObject particula;


    void Start()
    {
        jugador = GameObject.Find("Jugador");

    }

    private void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);

    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala2"))
        {
            GameObject particula2;
            particula2 = Instantiate(particula, transform.position, Quaternion.identity);
            Destroy(particula2, 1);
            Destroy(gameObject);
        }
        //else if (collision.gameObject.CompareTag("Jugador"))
        //{
        //    SceneManager.LoadScene("MainMenu");
        //}
    }

  

}
