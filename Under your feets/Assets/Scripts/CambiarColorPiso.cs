using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiarColorPiso : MonoBehaviour
{
  
    public Renderer render;
    public Color color1;
    public Color color2;
    void Start()
    {
    }


    void Update()
    {
    }

    private void cambiamosColor1()
    {
            render = GetComponent<Renderer>();
            render.material.color = color1;
    }
    private void cambiamosColor2()
    {
        render = GetComponent<Renderer>();
        render.material.color = color2;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala1"))
        {
            cambiamosColor1();
        }
        if (collision.gameObject.CompareTag("bala2"))
        {
            cambiamosColor2();
        }

    }
}

