using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuntaDeArma : MonoBehaviour
{

    public Image mira;


    void Start()
    {
        mira.color = Color.magenta;

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            mira.color = Color.magenta;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            mira.color = Color.yellow;

        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            mira.color = Color.green;

        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            mira.color = Color.blue;

        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            mira.color = Color.red;

        }
    }
}
