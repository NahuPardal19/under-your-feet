using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    private int cont;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    private Rigidbody rb;
    public Camera camaraPrimeraPersona;
    public GameObject proyectil1;
    public GameObject proyectil2;
    public GameObject proyectil3;
    public GameObject proyectil4;
    public GameObject proyectiltp;
    private bool arma1;
    private bool arma2;
    private bool arma3;
    private bool arma4;
    private bool armatp;
    public GameObject jugador;
    private List<GameObject> listaObjetos = new List<GameObject>();
    public float magnitudSaltoPiso;
    public Transform tpBlue;
    public Transform tpYellow;
    public Transform tpOrange;
    private bool TpBlueDesactivado;
    private bool TpYellowDesactivado;
    private bool TpOrangeDesactivado;

   
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        Cursor.lockState = CursorLockMode.Locked;
        arma1 = true;
        arma2 = false;
        arma3 = false;
        arma4 = false;
        armatp = false;
        TpOrangeDesactivado = true;
        TpBlueDesactivado = true;
        TpYellowDesactivado = true;


    }

    void Update()
    {



        //if (Input.GetKeyUp("escape"))
        //{
        //    Cursor.lockState = CursorLockMode.None;
        //}
        if (Input.GetKeyDown("escape"))
        {
            SceneManager.LoadScene("EscenaMenu");
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            Cursor.lockState = CursorLockMode.None;
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (EstaEnPiso())
            {
                rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            }

        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            arma2 = false;
            arma3 = false;
            armatp = false;
            arma4 = false;
            arma1 = true;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            arma1 = false;
            arma3 = false;
            armatp = false;
            arma4 = false;
            arma2 = true;
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            arma1 = false;
            arma2 = false;
            armatp = false;
            arma4 = false;
            arma3 = true;
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            arma1 = false;
            arma2 = false;
            arma3 = false;
            arma4 = false;
            armatp = true;
        }
        if(Input.GetKeyDown(KeyCode.Q))
        {
            arma1 = false;
            arma2 = false;
            arma3 = false;
            armatp = false;
            arma4 = true;
        }


        if (Input.GetMouseButtonDown(0) && arma1 )
        {
         
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil1, ray.origin, transform.rotation);



            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 50, ForceMode.Impulse);

            Destroy(pro, 5);

        }
        if (Input.GetMouseButtonDown(0) && arma2)
        {
          
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            GameObject pro;
            pro = Instantiate(proyectil2, ray.origin, transform.rotation);



            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 30, ForceMode.Impulse);
            Destroy(pro, 5);

        }
        if (Input.GetMouseButtonDown(0) && arma3)
        {
       
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil3, ray.origin, transform.rotation);



            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 30, ForceMode.Impulse);

            Destroy(pro, 5);

        }
        if (Input.GetMouseButtonDown(0) && armatp )
        {
         
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectiltp, ray.origin, transform.rotation);



            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 30, ForceMode.Impulse);

            Destroy(pro, 5);

        }
        if (Input.GetMouseButtonDown(0) && arma4)
        {
  
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil4, ray.origin, transform.rotation);



            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 30, ForceMode.Impulse);
            Destroy(pro, 5);

        }


  
    }

    private void FixedUpdate()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

    }


    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //if (collision.gameObject.CompareTag("impulso"))
        //{
        //    rb.AddForce(Vector3.up * magnitudSaltoPiso, ForceMode.Impulse);
        //}
        //if (collision.gameObject.CompareTag("piso2"))
        //{
        //    transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
        //}
        if (collision.gameObject.CompareTag("pisotpBlue") && TpBlueDesactivado)
        {
            jugador.transform.position = tpBlue.transform.position;
            TpBlueDesactivado = false;
        }
        if (collision.gameObject.CompareTag("pisotpYellow") && TpYellowDesactivado)
        {
            jugador.transform.position = tpYellow.transform.position;
            TpYellowDesactivado = false;
        }
        if (collision.gameObject.CompareTag("pisotpOrange") && TpOrangeDesactivado)
        {
            jugador.transform.position = tpOrange.transform.position;
            TpOrangeDesactivado = false;
        }
   

    }


}
